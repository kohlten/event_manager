use std::sync::Mutex;
use std::sync::Arc;
use std::collections::VecDeque;

pub struct EventManager<T> {
    events: Arc<Mutex<VecDeque<T>>>,
}

impl<T> EventManager<T> {
    pub fn new() -> Self {
        EventManager {
            events: Arc::new(Mutex::new(VecDeque::new())),
        }
    }

    pub fn push_back(&mut self, e: T) {
        let mut events = self.events.lock().unwrap();
        events.push_back(e);
    }

    pub fn push_front(&mut self, e: T) {
        let mut events = self.events.lock().unwrap();
        events.push_front( e);
    }

    // Generic for push_back.
    pub fn push(&mut self, e: T) {
        self.push_back(e);
    }

    pub fn pop_back(&mut self) -> Option<T> {
        let mut events = self.events.lock().unwrap();
        events.pop_back()
    }

    pub fn pop_front(&mut self) -> Option<T> {
        let mut events = self.events.lock().unwrap();
        events.pop_front()
    }

    // Generic for pop_back
    pub fn pop(&mut self) -> Option<T> {
        self.pop_back()
    }
}

unsafe impl<T> Send for EventManager<T> {}
unsafe impl<T> Sync for EventManager<T> {}

#[cfg(test)]
mod tests {
    use crate::manager::EventManager;

    #[test]
    fn test_manager() {
        let mut manager: EventManager<u32> = EventManager::new();
        manager.push_back(0);
        manager.push_back(1);
        manager.push_back(2);
        let event = manager.pop_front().unwrap();
        assert_eq!(event, 0);
        let event = manager.pop_front().unwrap();
        assert_eq!(event, 1);
        let event = manager.pop_front().unwrap();
        assert_eq!(event, 2);
    }
}